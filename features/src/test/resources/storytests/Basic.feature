@Happy
@Basic
Feature: Basic

	Scenario: Add to Inventory

		Given that the database does not contain "griffin-abc"
		When I call AddToInventory for "griffin-abc"
		Then a subsequent call to AddToInventory for "griffin-abc" will return an error

	Scenario: Requisition

		Given that the database contains only "griffin-def"
		When I call RequisitionFromInventory with department number 123
		Then it returns GriffinID "griffin-def"
