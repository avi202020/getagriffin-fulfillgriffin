FROM openjdk:14
COPY ./fulfillgriffin-main-1.0.jar /
CMD ["java", "-cp", "fulfillgriffin-main-1.0.jar", "fulfillgriffin.main.FulfillGriffin"]
