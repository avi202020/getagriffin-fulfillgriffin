package fulfillgriffin.main;

import static spark.Spark.*;
import com.mysql.jdbc.Driver;
//import oracle.jdbc.driver.OracleDriver;
import com.google.gson.Gson;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FulfillGriffin {

	private String jdbcPort;
	private String jdbcUrl;
	private String jdbcUser;
	private String jdbcPass;
	private Logger logger = LoggerFactory.getLogger(FulfillGriffin.class);

	/** The main method starts this microservice. */
	public static void main(String[] args) {

		FulfillGriffin fulfillGriffin = new FulfillGriffin();
		Logger logger = fulfillGriffin.logger;

		get("/ping", (req, res) -> {
			logger.info("Received ping");
			return "ping";
		});

		get("/RequisitionFromInventory", (req, res) -> {

			try {
				logger.info("Received request RequisitionFromInventory...");
				String deptNoStr = req.queryParams("DeptNo");
				if (deptNoStr == null) {
					throw new Exception("Missing DeptNo parameter");
				}
				int deptNo = 0;
				try {
					deptNo = Integer.parseInt(deptNoStr);
				} catch (NumberFormatException ex) {
					throw new Exception("DeptNo must be an integer");
				}

				String griffinId = fulfillGriffin.requisitionFromInventory(deptNo);

				RequisitionResponse response = new RequisitionResponse(griffinId);
				if (response == null) throw new RuntimeException("From new RequisitionResponse: returned null");

				Gson gson = new Gson();
	 			String json = json = gson.toJson(response);

				return json.toString();

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/AddToInventory", (req, res) -> {

			try {
				logger.info("Received request AddToInventory...");
				String griffinId = req.queryParams("GriffinId");
				if (griffinId == null) {
					throw new Exception("Missing GriffinId parameter");
				}

				fulfillGriffin.addToInventory(griffinId);
				return "added to inventory";

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/CancelRequisition", (req, res) -> {

			try {
				logger.info("Received request CancelRequisition...");
				String griffinId = req.queryParams("GriffinId");
				if (griffinId == null) {
					throw new Exception("Missing GriffinId parameter");
				}

				fulfillGriffin.cancelRequisition(griffinId);
				return "griffin requisition cancelled";

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/GetGriffinsAvailCount", (req, res) -> {

			try {
				logger.info("Received request GetGriffinsAvailCount...");
				int count = fulfillGriffin.getGriffinAvailableCount();
				return String.valueOf(count);
			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/ping", (req, res) -> {
			try {
				logger.info("Received ping...");
				return "ping";
			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});
	}

	static class RequisitionResponse {
		public String griffinId;
		public RequisitionResponse(String griffinId) { this.griffinId = griffinId; }
		public void setGriffinId(String griffinId) { this.griffinId = griffinId; }
		public String getGriffinId() { return this.griffinId; }
	}

	private FulfillGriffin() {

		System.err.println("Initializing FulfillGriffin");

		this.jdbcPort = System.getenv("FULFILL_GRIFFIN_MYSQL_HOST_PORT");
		this.jdbcUrl = "jdbc:mysql://db:" + this.jdbcPort + "/FulfillGriffin?connectTimeout=0&socketTimeout=0&autoReconnect=true";
		//this.jdbcUrl = "jdbc:mysql://localhost:" + this.jdbcPort + "/FulfillGriffin?connectTimeout=0&socketTimeout=0&autoReconnect=true";
		this.jdbcUser = System.getenv("MYSQL_USER");
		this.jdbcPass = System.getenv("MYSQL_PASSWORD");

		if (this.jdbcPort == null) throw new RuntimeException("Env variable FULFILL_GRIFFIN_MYSQL_HOST_PORT not set");
		if (this.jdbcUser == null) throw new RuntimeException("Env variable MYSQL_USER not set");
		if (this.jdbcPass == null) throw new RuntimeException("Env variable MYSQL_PASSWORD not set");
	}

	/**
	Attempt to remove a griffin from inventory, and return the ID of the griffin.
	(The ID is actually the griffin's name.) If there are none, throw an exception.
	*/
	private String requisitionFromInventory(int deptNo) throws Exception {

		/* Start a transaction. For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		System.err.println("FulfillGriffin JDBC url=" + jdbcUrl);
		System.err.println("jdbcUser=" + jdbcUser);
		System.err.println("jdbcPass=" + jdbcPass);
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillGriffin database");
		conn.setAutoCommit(false);

		try {
			/* Find a table row whose "DeptNo" field is empty. */
			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet resultSet = stmt.executeQuery(
				"SELECT GriffinId, DeptNo FROM FulfillGriffin " +
				"WHERE (DeptNo IS NULL OR DeptNo = 0) LIMIT 1");

			String griffinId = null;
			for (; resultSet.next();) {
				griffinId = resultSet.getString(1);  // get the GriffinId
			}
			if (griffinId == null) throw new Exception("There are no more Griffins in inventory");

			/* Set the "DeptNo" field to deptNo. */
			Statement stmt2 = conn.createStatement();
			stmt2.executeUpdate("UPDATE FulfillGriffin SET DeptNo = " + deptNo +
				" WHERE GriffinId = '" + griffinId + "'");

			/* Commit the transaction. */
			conn.commit();

			return griffinId;

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}

	/**
	Add or return the specified griffin to inventory.
	*/
	private void addToInventory(String griffinId) throws Exception {

		/* Start a transaction. For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillGriffin database");
		conn.setAutoCommit(false);

		try {
			/* Check if that griffin is already in the database. */
			Statement stmt1 = conn.createStatement();
			ResultSet resultSet = stmt1.executeQuery(
				"SELECT DeptNo FROM FulfillGriffin where GriffinId = '" + griffinId + "'");
			for (; resultSet.next();) {
				throw new Exception("Database already contains griffin '" + griffinId + "'");
			}

			/* Insert the griffin. */
			Statement stmt2 = conn.createStatement();
			stmt2.executeUpdate("INSERT INTO FulfillGriffin VALUES ('" + griffinId + "', 0)");

			conn.commit();

		} catch (Exception ex2) {
			try {
				conn.rollback();
			} catch (Exception ex) {
				logger.debug("On rollback; msg=" + ex.getMessage(), ex);
				throw ex;
			}
			throw ex2;
		} finally {
			conn.close();
		}
	}

	private void cancelRequisition(String griffinId) throws Exception {

		/* Start a transaction. For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillGriffin database");

		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE FulfillGriffin SET DeptNo = 0" +
				" WHERE GriffinId = '" + griffinId + "'");

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}

	private int getGriffinAvailableCount() throws Exception {

		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillGriffin database");
		try {
			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(
				"SELECT count(*) FROM FulfillGriffin WHERE DeptNo = 0");
			resultSet.next();
			int count = resultSet.getInt(1);
			return count;

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}
}
