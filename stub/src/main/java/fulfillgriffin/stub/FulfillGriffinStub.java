package fulfillgriffin.stub;

/* */
public interface FulfillGriffinStub {
	String requisitionFromInventory(int deptNo) throws Exception;
	void cancelRequisition(String griffinId) throws Exception;
	void addToInventory(String griffinId) throws Exception;
	int getNoOfGriffinsAvailable() throws Exception;
	String ping() throws Exception;
}
