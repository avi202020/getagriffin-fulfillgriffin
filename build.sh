# Optional parameters: DOCKERHUB_USER_ID DOCKERHUB_PASSWORD

mvn clean compiler:compile jar:jar shade:shade install:install
if [ $? -ne 0 ]; then
	exit 1
fi

# If on Linux, then create container image:
if [ `uname` = 'Linux' ]; then
	mkdir -p scratch
	cp main/target/*.jar scratch
	echo "Building image"
	sudo docker rmi ${IMAGE_NAME}  # delete the old one - this might fail
	sudo docker build -t ${IMAGE_NAME} -f Dockerfile scratch
	if [ $? -ne 0 ]; then
		exit 1
	fi
	if [ "$#" -eq 2 ]; then
		echo "Pushing image to Dockerhub"
		sudo docker login -u $1 -p $2
		sudo docker push ${IMAGE_NAME}
	fi
	rm -r scratch
fi
